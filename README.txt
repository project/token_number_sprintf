Use sprintf (https://www.php.net/manual/en/function.sprintf.php) format strings to format
number field values. Do not wrap the string in quotation marks. Note that this is a
simple application of the function using a single value:

     sprintf($your_format_string, $numeric_field_value);

A formatting string that expects multiple values will produce no output.
