<?php

/**
 * @file
 * Provides a custom format token for number fields.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_token_info().
 */
function token_number_sprintf_token_info() {
  if (!\Drupal::hasService('token.entity_mapper')) {
    return;
  }

  $allowed_field_types = [
    'decimal',
    'float',
    'integer',
  ];

  $types = [];
  $tokens = [];
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }
    $token_type = \Drupal::service('token.entity_mapper')->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type)) {
      continue;
    }

    // Build custom format tokens for all number fields.
    $fields = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);
    foreach ($fields as $field_name => $field) {
      if (!in_array($field->getType(), $allowed_field_types)) {
        continue;
      }

      $tokens[$token_type . '-' . $field_name]['formatted'] = [
        'name' => t('Formatted'),
        'description' => t('Format the value using sprintf.'),
        'module' => 'token_number_sprintf',
        'dynamic' => TRUE,
      ];
    }
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function token_number_sprintf_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if (!empty($data['field_property'])) {
    foreach ($tokens as $token => $original) {
      $delta = 0;
      $parts = explode(':', $token);
      $format = end($parts);
      if (is_numeric($parts[0]) and count($parts) > 1) {
        $delta = $parts[0];
      }
      if (!isset($data[$data['field_name']][$delta])) {
        continue;
      }
      $field_item = $data[$data['field_name']][$delta];
      $replacements[$original] = sprintf($format, $field_item->getValue()['value']);
    }
  }

  return $replacements;
}
